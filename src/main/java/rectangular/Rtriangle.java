package rectangular;

public class Rtriangle {	
	
	private double apexX1;       //Координаты вершин треугольника
	private double apexX2;
	private double apexX3;
	private double apexY1;
	private double apexY2;
	private double apexY3;
	
	Rtriangle (double x1, double y1, double x2, double y2, double x3, double y3) {   // Конструктор (координаты вершины №1, координаты вершины №2, координаты вершины №3)
		
		this.setApexX1(x1);
		this.setApexY1(y1);
		this.setApexX2(x2);
		this.setApexY2(y2);
		this.setApexX3(x3);
		this.setApexY3(y3);
	}
 
	double getApexX1() {                // Геттеры и сеттеры
		return apexX1;
	}

	void setApexX1(double apexX1) {
		this.apexX1 = apexX1;
	}

	double getApexX2() {
		return apexX2;
	}

	void setApexX2(double apexX2) {
		this.apexX2 = apexX2;
	}

	double getApexX3() {
		return apexX3;
	}

	void setApexX3(double apexX3) {
		this.apexX3 = apexX3;
	}

	double getApexY1() {
		return apexY1;
	}

	void setApexY1(double apexY1) {
		this.apexY1 = apexY1;
	}

	double getApexY2() {
		return apexY2;
	}

	void setApexY2(double apexY2) {
		this.apexY2 = apexY2;
	}

	double getApexY3() {
		return apexY3;
	}

	void setApexY3(double apexY3) {
		this.apexY3 = apexY3;
	}


}
