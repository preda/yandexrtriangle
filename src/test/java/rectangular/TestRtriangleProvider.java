package rectangular;

import static org.junit.Assert.*;
import org.junit.*;

public class TestRtriangleProvider {
	
	public static Rtriangle rtriangle;
	public static double x1, x2, x3, y1, y2, y3;
	
	@BeforeClass
	public static void setUp() {
	
		rtriangle = RtriangleProvider.getRtriangle(); // Получаем прямоугольный треугольник 
		assertNotNull("Rtriangle is null!", rtriangle); 
		x1 = rtriangle.getApexX1();  // Получаем координаты вершин
		x2 = rtriangle.getApexX2();
		x3 = rtriangle.getApexX3();
		y1 = rtriangle.getApexY1();
		y2 = rtriangle.getApexY2();
		y3 = rtriangle.getApexY3();
	}
	
	@Test
	public void testGetRtriangle () {
			
		double hypotenuse, cathetus1, cathetus2; // Гипотенуза, 2 катета - их необходимо определить
		double side1, side2, side3; // Стороны треугольника
		side1 = getLenght(x1, y1, x2, y2);
		side2 = getLenght(x1, y1, x3, y3);
		side3 = getLenght(x3, y3, x2, y2);
		assertTrue(side1!=0 & side2!=0 & side3!=0); // Проверяем, что нет сторон равных 0.
		
		if (side1 > side2) {  // Определяем гипотенузу и катеты
			if (side1 > side3) {
				hypotenuse = side1;
				cathetus1 = side2;
				cathetus2 = side3;
			} else {
				hypotenuse = side3;
				cathetus1 = side2;
				cathetus2 = side1;
			}
		} else {
			if (side2 > side3) {
				hypotenuse = side2;
				cathetus1 = side1;
				cathetus2 = side3;
			} else {
				hypotenuse = side3;
				cathetus1 = side2;
				cathetus2 = side1;
			}
		}
		
		System.out.println(side1 + " " + side2 + " " + side3);
		System.out.println(x1 + " " + y1 + "  " + x2 + " " + y2 + "  " + x3 + " " + y3);
		assertEquals(Math.pow(hypotenuse, 2.0), Math.pow(cathetus1, 2.0) + Math.pow(cathetus2, 2.0), 0.00001); // Проверяем по теореме Пифагора
	}
	
	public double getLenght(double x1, double y1, double x2, double y2) { // Метод вычисления длины одной из сторон треугольника
		double lenght = 0;
		lenght = Math.sqrt(Math.pow((x2-x1),2.0) + Math.pow((y2-y1),2.0));
		return lenght;
	}
}
